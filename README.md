# Number of Crosses Prediction

This project contains a simple analysis of Premier League dataset and a Regression model to determine the number of crosses 
from a player in one match.

## Feature Importance
Following are the features that will be used to determine the number of crosses. Unique variables and some variables that are only 
known once the game is completed will be removed.

### 1. Removed variables
1. Date 
2. Player (unique Value)
3. PlyID (Unique Value)
4. Competition (only contained EPL)
5. Team, Opponent, Manager, HmAw (This feature will be summarize into one segment called 'Teaam Strategy')
6. Team.Possesion (only known once the game is completed)
7. Team.Goals (only known once the game is completed)
8. Opponent.Goal (only known once the game is completed)
9. Minutes (only known once the game is completed)

### 2. WinProb features
Here are some of the scatter plots between WinProb, possesion and number of Crosses in one game :

![ImgName](https://bitbucket.org/barualam/premierleague/raw/9a8ee143f6693f07035a1022e52475e7a2f6f640/src/Cross%20Vs%20WinProb.PNG)
![imgName](https://bitbucket.org/barualam/premierleague/raw/9a8ee143f6693f07035a1022e52475e7a2f6f640/src/poss%20vs%20Cross.PNG)
![imgName](https://bitbucket.org/barualam/premierleague/raw/9a8ee143f6693f07035a1022e52475e7a2f6f640/src/poss%20vs%20winprob.PNG)

As we can see that the team's possession correlated with the number of Crosses in one game, with a correlation of 0.5,
However the Team.Possesion variable will only be known once the game is completed, so we can't use it in the model that will be created. 
On the other hand WinProb and Team Possesion variables are highly correlated, with a correlation of 0.7. So it's reasonable
to put this WinProb variable as one of the important features.

### 3. Team Strategy
Every Football team has their own manager with different tactics. As a reason for that, I have created a new feature by creating a segmentation
based on the Team's average number of crosses.

![imgName](https://bitbucket.org/barualam/premierleague/raw/c27fedc28e792b4691c6a66e9aa4d860e14bb621/src/Average%20Crosses.PNG)

* less crosses strategy : Teams with average number of crosses less than 11
* average crosses strategy : Teams with average number of crosses between 11 and 14
* high crosses strategy : Teams with average number of crosses higher than 14

### 4. Player Position
Players who occupy the wing's position tend to give more crosses than the players who occupy center-attackers, midfielders, defenders or goal keepers. 

![imgName](https://bitbucket.org/barualam/premierleague/raw/9ebae208d5b1fdd9a797d824327f250a3569d063/src/Player%20Position.PNG)

### 5. Special Player
Some players have an extremely high number of crosses in every match compared to the other players on the same position. 
Here is an example : On 'AMC' position, the average crosses in one match is 1.2 Crosses per game, but some players have an average 
crosses much higher than it.

![imgName](https://bitbucket.org/barualam/premierleague/raw/9ebae208d5b1fdd9a797d824327f250a3569d063/src/boxplot.PNG)
![imgName](https://bitbucket.org/barualam/premierleague/raw/9ebae208d5b1fdd9a797d824327f250a3569d063/src/player%20habit.PNG)

Thus, I have created a rule for this kind of special player: 

- 0 : for players with an average number of crosses less than q3 + 1.5 IQR (outlier) 
- 1 : for players with an average number of crosses bigger than q3 + 1.5 IQR (outlier)
- 2 : for players with an average number of crosses bigger than 2 * (q3 + 1.5 IQR)


## Model Evaluation

Provided below is the result of the models that have been created :

![imgName](https://bitbucket.org/barualam/premierleague/raw/aba06c5afa4ad670403bc9b409d19507d2c3d32d/src/model_evaluations.PNG)

Based from the result, it is clear that Random Forest regression gave the best result from all of the evaluation metrics.